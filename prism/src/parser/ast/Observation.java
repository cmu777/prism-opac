
package parser.ast;

import parser.visitor.*;
import prism.PrismLangException;

/**
 * Variable declaration details
 */
public class Observation extends ASTElement
{
	// Transition label Name
	protected String name;
	// observation of the label
	protected String observedLbl;
		
	public Observation(String name, String observedLbl)
	{
		setName(name);
		if (observedLbl.compareTo("null") == 0)
			observedLbl = "";
		setObservedLbl(observedLbl);
	}
	
	// Set methods
	
	public void setName(String name)
	{
		this.name = name;
	}	

	public void setObservedLbl(String observedLbl)
	{
		if (observedLbl.compareTo("null") == 0)
			observedLbl = "";
		this.observedLbl = observedLbl;
	}	

	// Get methods

	public String getName()
	{
		return name;
	}

	public String getObservedLbl()
	{
		return observedLbl;
	}	
	
	// Methods required for ASTElement:
	
	/**
	 * Visitor method.
	 */
	public Object accept(ASTVisitor v) throws PrismLangException
	{
		return v.visit(this);
	}

	/**
	 * Convert to string.
	 */
	@Override
	public String toString()
	{
		String s  = "";
		s += name + " -> ";
		s += observedLbl;
		return s;
	}

	/**
	 * Perform a deep copy.
	 */
	@Override
	public ASTElement deepCopy()
	{
		Observation ret = new Observation(getName(), getObservedLbl());
		ret.setPosition(this);
		return ret;
	}
}

// ------------------------------------------------------------------------------
