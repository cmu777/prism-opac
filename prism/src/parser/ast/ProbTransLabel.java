
package parser.ast;

import parser.visitor.*;
import prism.PrismLangException;

/**
 * Variable declaration details
 */
public class ProbTransLabel extends ASTElement
{
	// Probability of the transition label
	protected double value = 0.0;
	// name of the transition label
	protected String name = "";
	protected String observation = "";
	protected int from = 0; // from state
	protected int to = 0; // to state
		
	public ProbTransLabel(String n, double v)
	{
		setName(n);
		setValue(v);
	}
	
	public ProbTransLabel(String n, String o, double v)
	{
		setName(n);
		setValue(v);
		setObservation(o);
	}
	
	public ProbTransLabel(String n, double v, int f, int t)
	{
		setName(n);
		setValue(v);
		setFrom(f);
		setTo(t);
	}
	
	public ProbTransLabel(String n, String o, double v, int f, int t)
	{
		setName(n);
		setValue(v);
		setObservation(o);
		setFrom(f);
		setTo(t);
	}
	
	// Set methods
	
	public void setAll(String n, String o, double v, int f, int t) 
	{
		this.name = n;
		this.observation = o;
		this.value = v;
		this.from = f;
		this.to = t;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}	
	
	public void setValue(double v)
	{
		this.value = v;
	}	
	
	public void setObservation(String o)
	{
		this.observation = o;
	}

	public void setFrom(int f)
	{
		this.from = f;
	}	
	
	public void setTo(int t)
	{
		this.to = t;
	}	
	
	// Get methods

	public String getName()
	{
		return name;
	}

	public double getValue()
	{
		return value;
	}	
	
	public String getObservation()
	{
		return observation;
	}
	
	public int getFrom()
	{
		return from;
	}

	public int getTo()
	{
		return to;
	}
	
	@Override
    public boolean equals(Object obj) {

        try {
        	ProbTransLabel ptl  = (ProbTransLabel) obj;
            return name.equals(ptl.getName());
        }
        catch (Exception e)
        {
            return false;
        }

    }

	
	// Methods required for ASTElement:
	
	/**
	 * Visitor method.
	 */
	public Object accept(ASTVisitor v) throws PrismLangException
	{
		return v.visit(this);
	}

	/**
	 * Convert to string.
	 */
	@Override
	public String toString()
	{
		String s  = "";
		s += from + "->" + to + ":";
		s += value + ":";
		s += name;
		s += ":" + observation;
		return s;
	}

	/**
	 * Perform a deep copy.
	 */
	@Override
	public ASTElement deepCopy()
	{
		ProbTransLabel ret = new ProbTransLabel(getName(), getObservation(), getValue(), getFrom(), getTo());
		ret.setPosition(this);
		return ret;
	}
}

// ------------------------------------------------------------------------------
